COMPILER := as
CMPFLAGS :=
CMPFLAGSDEB := --gdwarf2

LINKER := ld
LNKFLAGS := -static

.PHONY: all clean

all: main main_debug

main: main.o
	$(LINKER) $(LNKFLAGS) -o main main.o

main.o: main.s
	$(COMPILER) $(CMPFLAGS) -o main.o -c main.s

main_debug: main_debug.o
	$(LINKER) $(LNKFLAGS) -o main_debug main_debug.o

main_debug.o: main.s
	$(COMPILER) $(CMPFLAGSDEB) -o main_debug.o -c main.s

clean:
	rm main; rm main_debug; rm *.o
