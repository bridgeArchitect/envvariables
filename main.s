.include "defs.h"

.section .bss
/* variable for pointer of environment */
envp: .quad 0

.section .text

.global _start

/* symbol of new line */
newline:
.byte '\n'

_start:

/*
 * Layout of arguments on stack:
 *
 *   0(%rsp)  - argc
 *   8(%rsp)  - argv[0]
 *   ...      - argv[argc - 1]
 *   NULL
 *   ...      - envp[0] - environment
 *   ...      - envp[n - 1]
 *   NULL
 */

/* It is important to know:
 * %rax - type of system call
 * %rdi - thread of data (if system call is printing)
 * %rdi - value to return (if system call is exiting)
 * %rsi - row to print (if system call is printing)
 * %rdx - length of row to print (if system call is printing)
 * %rsp - pointer to beginning of system stack
 */

	movq (%rsp), %rbx             /* %rbx = *%rsp */
	leaq 16(%rsp, %rbx, 8), %rcx  /* %rcx = %rsp + 8%rbx + 16 (rbx=argc) */
	movq %rcx, envp               /* envp = %rcx */

loop:

        movq envp, %rcx               /* rcx = envp */
        movq (%rcx), %rsi             /* rsi = *envp */
        movq %rsi, %rdi               /* rdi = rsi */
        movq $0, %rdx                 /* rdx = 0 */

strlen:
        cmpb $0, (%rdi)               /* while (*rdi != '\0') */
        je cont                       /*  {  */
        incq %rdi                     /* rdi++ */
        incq %rdx                     /* rdx++ */
        jmp strlen                    /*  }*/

cont:

	/* write row (environment variable) */
        movq $SYS_WRITE, %rax         /* type of system call */
        movq $STDOUT, %rdi            /* thread of data */
        syscall                       /* make system call */

        addq $8, envp                 /* envp = envp + 8 (pointer size) */

	movq  envp, %rax              /* %rax = envp */
        cmpq  $0, (%rax)              /* if (*envp) == '/0' ... */
        je end                        /*   ... goto end */

        /* write newline */
        movq $SYS_WRITE, %rax         /* type of system call */
        movq $STDOUT, %rdi            /* thread of data */
        movq $newline, %rsi           /* data to write */
        movq $1, %rdx                 /* length of data */
        syscall                       /* make system call */

        jmp loop                      /* jump to begin of loop */

end:

        /* write newline */
        movq $SYS_WRITE, %rax         /* type of system call */
        movq $STDOUT, %rdi            /* thread of data */
        movq $newline, %rsi           /* data to write */
        movq $1, %rdx                 /* length of data */
        syscall                       /* make system call */

	/* exit program */
        movq $SYS_EXIT, %rax          /* type of system call */
        movq $0, %rdi                 /* value to return */
        syscall                       /* make system call */

